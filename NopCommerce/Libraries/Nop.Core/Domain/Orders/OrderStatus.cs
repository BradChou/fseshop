namespace Nop.Core.Domain.Orders
{
    /// <summary>
    /// Represents an order status enumeration
    /// </summary>
    public enum OrderStatus
    {
        /// <summary>
        /// Pending
        /// </summary>
        Pending = 10,

        /// <summary>
        /// Processing
        /// </summary>
        Processing = 20,

        /// <summary>
        /// Complete
        /// </summary>
        Complete = 30,

        /// <summary>
        /// Cancelled
        /// </summary>
        Cancelled = 40
    }

    /// <summary>
    /// Represents an Chinese order status enumeration
    /// </summary>
    public enum OrderStatusChinese
    {
        /// <summary>
        /// 等待
        /// </summary>
        等待 = 10,

        /// <summary>
        /// 處理中
        /// </summary>
        處理中 = 20,

        /// <summary>
        /// 完成
        /// </summary>
        完成 = 30,

        /// <summary>
        /// 已取消
        /// </summary>
        已取消 = 40
    }
}
